import os
from datetime import date, timedelta
from dateutil import rrule
import pyodbc
import pandas as pd
import zipfile
import requests
import json
import calendar

global nemo

start_date = date(2011,7,1)
end_date = date(2011,8,31)
nemo = 'PARTE_SEMANAL_UNIF'
pathCsv = 'data/csv/'
#https://cammesaweb.cammesa.com/parte-semanal/

def loteProceso(nemo,start_date,end_date):
        
    for dt in rrule.rrule(rrule.MONTHLY, dtstart=start_date, until=end_date):
        print(str(dt.year)+'-'+str(dt.month))
        days = calendar.monthrange(dt.year,dt.month)
        
        fechadesde = str(dt.year) + '-' + str(dt.month).zfill(2) + '-01'
        fechahasta = str(dt.year) + '-' + str(dt.month).zfill(2) + '-' + str(days[1]).zfill(2)

        getData(nemo,fechadesde,fechahasta)


def getData(nemo,fechadesde,fechahasta):
    
    print({'nemo': nemo,'fechadesde': fechadesde + 'T03:00:00.000Z','fechahasta': fechahasta + 'T03:00:00.000Z'})
    url = "https://api.cammesa.com/pub-svc/public/findDocumentosByNemoRango"
    jsonData = requests.get(url, params = {'nemo': nemo,'fechadesde': fechadesde + 'T03:00:00.000Z','fechahasta': fechahasta + 'T03:00:00.000Z'}, proxies=proxies)
    
   
    df = pd.read_json(jsonData.content)
    lastVersion = df.version.max()
    rslt_df = df[(df['nemo'] == 'PARTE_SEMANAL') & (df['version'] == lastVersion)] 
    index = rslt_df.index[0]
    adjuntos = dict(rslt_df['adjuntos'])[index]
    
    attachmentId = adjuntos[0]['nombre']
    docId = rslt_df['id']
    
    url = "https://api.cammesa.com/pub-svc/public/findAttachmentByNemoId"
    data = requests.get(url, params = {'nemo': nemo,'attachmentId': attachmentId,'docId': docId}, proxies=proxies)
    open('data/'+attachmentId, 'wb').write(data.content)

def mdb2csv(path,nameMdb):
    connStr = (
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};"
        r"DBQ="+path+'/'+nameMdb+".MDB;"
        )

    ### Connect to the database
    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()

    ### List all tables
    """for row in cursor.tables():
        print(f'Table: {row.table_name}')"""

    sql = "SELECT * FROM Q_Parsem"
    data = pd.read_sql(sql,cnxn)
    data.to_csv(pathCsv + nameMdb + '.csv',index=False)

    ### Close the connection to the file. 
    cursor.close()
    cnxn.close()   


loteProceso(nemo,start_date,end_date)

for file in os.listdir("data/"):
    if file.endswith(".zip"):
        print(os.path.join("data/", file))
        with zipfile.ZipFile(os.path.join("data/", file), 'r') as zip_ref:
            zip_ref.extractall(os.path.join("data/unzip", file))
            mdb2csv(os.path.join("data/unzip", file),os.path.splitext(file)[0])
