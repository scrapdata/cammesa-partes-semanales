
# Scrap Partes Semanales CAMMESA

Permite tomar la información de los partes semanales de CAMMESA, los cuales se encuentran dentro de un ACCESS (MDB) con una versión antigua, por lo que es necesario el driver "Microsoft Access Driver" version 14.00, dejando una salida CSV por cada tabla que contiene el MDB.


## Variables

Dentro del script se debe establecer el rango a realizar el scrapeo:

`start_date` -> Fecha de inicio

`end_date` -> Fecha final

`pathCsv` -> Directorio donde dejara los CSV (default 'data/csv/')

`nemo` -> Es el tipo de documento a descargar (defaul 'PARTE_SEMANAL_UNIF')


## Links

 - [Origenes de datos](https://cammesaweb.cammesa.com/parte-semanal/)


## Instalación

Primero se debe tener instalada la versión de driver de Access especificada.

```bash
  python -m venv venv
  venv/Scripts/activate
  pip install -r requirements.txt
  python main.py
```
    